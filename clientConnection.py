#/usr/bin/python
import ssl
import websocket
import hashlib
import subprocess
import vlc
import os
import signal
import time


from subprocess import call
from uuid import getnode as get_mac
from multiprocessing import Pool

try:
    import thread
except ImportError:
    import _thread as thread

#https://www.olivieraubert.net/vlc/python-ctypes/doc/vlc.Instance-class.html
#https://github.com/geoffsalmon/vlc-python
#https://docs.python.org/3/library/multiprocessing.html#module-multiprocessing
#https://www.saltycrane.com/blog/2009/10/how-capture-stdout-in-real-time-python/




def finishSearching(connected):
    ws.scan = False
    ws.doneScan = True
    channels = convertFromFile("channels.txt")
    channelsString = "NRK 2\n"
    for channel in channels:
        channelsString += channel[0] + "\n"
    print(channelsString)
    ws.send("stations:" + channelsString)

def startAsyncSearch(path,connected):
    print("hallo")
    cmd = 'w_scan -c NO -ft -X >> ' + path + '/channels.txt'
    scan = subprocess.Popen(cmd, shell=True)
    #time.sleep(10)
    scan.wait()
    return connected


def convertFromFile(filename):
    try:
        file = open(filename, 'r')
        stringfromfile = file.readlines()
        tab = []
        restab = []
        for element in stringfromfile:
            tab.append(element.split(":"))
        for element in tab:
            print(element[0])
            restab.append([element[0], element[1]])
        return restab

    except FileNotFoundError:
        print("File not found exception in formatToArray function!")
        return

def on_message(ws, message):
    print(message)
    data = message.split(":")
    identifier = data[0]
    data_length = len(data)
    channels = []

    if data_length == 2 and identifier == "authenticated":
        if data[0] == "true":
            ws.connected = True
            print("successfully connected")
        elif data[0] == "false":
            ws.connected = False
            print("Did not connect")
    elif identifier == "scan":
        ws.scan = True
        ws.doneScan = False
        ws.pool.apply_async(startAsyncSearch, [path,ws.connected], callback=finishSearching)

    elif identifier == "getStations":
        if ws.doneScan:
            channels = convertFromFile("channels.txt")
            channelsString = ""
            for channel in channels:
                channelsString += str(channel[0])+"\n"
            ws.send("stations:NRK 2\n"+channelsString)
        else:
            ws.send("stations:NRK 2")

    elif data_length == 2 and identifier == "playStation":
        if ws.scan:
            print("Scanning")
        else:
            if (data[1] == "NRK 2"):
                m = ws.i.media_new('dvb-t://frequency=746000000')
                ws.p.set_media(m)
                ws.p.play()
            else:
                print("Encrypted channel!")

    elif data_length == 2 and identifier == "volume":
        if data[1] == "mute":
            ws.p.pause()
            call(["amixer", "-q", "sset", "Master", "toggle"])
        elif data[1] == "up":
            call(["amixer", "-q", "sset", "Master", "7%+"])
        elif data[1] == "down":
            call(["amixer", "-q", "sset", "Master", "7%-"])
    elif data_length == 2 and identifier == "room":
        if data[1] == "false":
            try:
                ws.p.release()
            except Exception:
                print("Did not release the player!!")
        elif data[1] == "true":
            print("added to room!")
    else:
        print("Unknown command: "+message)



def on_error(ws, error):
    if hasattr(ws,"p"):
        ws.p.release()
    if hasattr(ws,"searchProcess"):
        os.killpg(os.getpgid(ws.searchProcess.pid), signal.SIGTERM)
    if hasattr(ws,"searchProcess"):
        os.killpg(os.getpgid(ws.searchProcess.pid), signal.SIGTERM)
    if hasattr(ws,"pool"):
        ws.pool.close()
    if hasattr(ws,"result"):
        print(ws.result)
    print("OnError: ")
    print(error)

def on_close(ws):
    if hasattr(ws, "p"):
        ws.p.release()
    if hasattr(ws, "pool"):
        ws.pool.close()
    print("### closed ###")

def on_open(ws):
    def run(*args):
        ws.connected = False
        ws.scan = False
        ws.connected = True
        ws.doneScan = False
        # initiate a vlc instance and create a player
        ws.i = vlc.Instance('--fullscreen')
        ws.p = ws.i.media_player_new()

        print(path)

        ws.pool = Pool(processes=1)

        # check if there exists a channel file if not scan
        if os.path.exists(path+'/channels.txt'):
            ws.doneScan = True
            print("set done scan to true")
        #else:
           #ws.pool.apply_async(startAsyncSearch, [path, False], callback=finishSearching)


        #scan on startup Firstime scan:
        #ws.pool.apply_async(startAsyncSearch, [path, False], callback=finishSearching)


        fn = 'auth.txt'
        try:
            file = open(fn, 'r')
            print("found file!")
        except FileNotFoundError:
            mac = get_mac()
            print(mac)
            m = hashlib.sha256()
            m.update(str(mac).encode('utf   -8'))
            file = open(fn, 'w+')
            file.write(m.hexdigest())
            print("created file!")
            file = open(fn, 'r')

        content = file.readlines()

        hash = content[0]
        ws.send("imaPlayer:"+hash)

        #Get channels from file
        fn = 'channels.txt'

    thread.start_new_thread(run, ())


if __name__ == "__main__":
    # get path:
    path = os.path.dirname(os.path.abspath(__file__))

    # websocket.enableTrace(True)
    ws = websocket.WebSocketApp("wss://18.188.200.130:3000",
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    ws.on_open = on_open
    #while True:
    ws.run_forever(sslopt={"cert_reqs": ssl.CERT_NONE})
        #print("Disconnected! trying to reconnect!")
        #time.sleep(3)
