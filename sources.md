

# Sources

The following sources are used or provide inspiration:

* [1]“vlc.Instance.” [Online]. Available: https://www.olivieraubert.net/vlc/python-ctypes/doc/vlc.Instance-class.html. [Accessed: 23-May-2018].
* [2]G. Salmon, vlc-python: VLC’s python bindings. 2018. [Online] https://www.olivieraubert.net/vlc/python-ctypes/doc/vlc.Instance-class.html[Accessed: 23-May-2018].
* [3]“17.2. multiprocessing — Process-based parallelism — Python 3.6.5 documentation.” [Online]. Available: https://docs.python.org/3/library/multiprocessing.html#module-multiprocessing. [Accessed: 23-May-2018].
* [4]“How to capture stdout in real-time with Python - SaltyCrane Blog.” [Online]. Available: https://www.saltycrane.com/blog/2009/10/how-capture-stdout-in-real-time-python/. [Accessed: 23-May-2018].